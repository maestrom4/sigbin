int incomingByte = 0;
int servo0 = 600;
int servo180 = 2100;
int inc = 20;
int pos = servo0;
int servoPin = 9;
int pulseInterval=2000;

void setup() {
  Serial.begin(9600);     // opens serial port, sets data rate to 9600 bps
  pinMode(servoPin, OUTPUT);
}

void loop() {
  int i;

  pos += inc;

  if (pos > servo180) {
    Serial.println("REVERSE!");
    pos = servo180;
    inc *= -1;
    delay(500);
  } else if (pos < servo0) {
    Serial.println("FORWARD!");
    pos = servo0;
    inc *= -1;
    delay(500);
  }

  Serial.print("pos = ");
  Serial.println(pos, DEC);

  digitalWrite(servoPin, HIGH);
  delayMicroseconds(pos);
  digitalWrite(servoPin, LOW);
  delay(20);
}
