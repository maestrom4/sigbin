// defines pins numbers
const int trigPin = 26;
const int echoPin = 27;
//#include "esp32-hal-ledc.h" //servo
// defines variables
long duration;
int distance;
long inches;
//start of speaker
int freq = 2000;
int channel = 0;
int resolution = 8;
int speakerpin = 15;
void setup() {
pinMode(trigPin, OUTPUT); // Sets the trigPin as an Output
pinMode(echoPin, INPUT); // Sets the echoPin as an Input
Serial.begin(115200); // Starts the serial communication
  /*Speaker*/
  ledcSetup(channel, freq, resolution);
  ledcAttachPin(speakerpin, channel);
}
void alarmFreq(){
  for (int freq = 0; freq < 4; freq = freq + 1){
   Serial.println(freq);
   ledcWriteTone(channel, 510);
   delay(500);
   ledcWriteTone(channel, 610);
   delay(500);
  }
  delay(2000);
  ledcWriteTone(channel, 0);
}
void loop() {
// Clears the trigPin
digitalWrite(trigPin, LOW);
delayMicroseconds(2);

// Sets the trigPin on HIGH state for 10 micro seconds
digitalWrite(trigPin, HIGH);
delayMicroseconds(10);
digitalWrite(trigPin, LOW);

// Reads the echoPin, returns the sound wave travel time in microseconds
duration = pulseIn(echoPin, HIGH);

// Calculating the distance
distance= duration*0.034/2;

// Prints the distance on the Serial Monitor
Serial.print("Distance in inches: ");
//Serial.println(distance);
inches = microsecondsToInches(duration);
Serial.println(inches);
  if(inches<5){
    alarmFreq();
    alarmFreq();
  }
}


long microsecondsToInches(long microseconds)
{
  // According to Parallax's datasheet for the PING))), there are
  // 73.746 microseconds per inch (i.e. sound travels at 1130 feet per
  // second).  This gives the distance travelled by the ping, outbound
  // and return, so we divide by 2 to get the distance of the obstacle.
  // See: http://www.parallax.com/dl/docs/prod/acc/28015-PING-v1.3.pdf
  return microseconds / 74 / 2;
}
