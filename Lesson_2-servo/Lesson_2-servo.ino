 #define COUNT_LOW 0
 #define COUNT_HIGH 8888 

 #define TIMER_WIDTH 16

#include "esp32-hal-ledc.h"
int LED_SET=5;
void setup() {
   Serial.begin(115200);
   ledcSetup(1, 50, TIMER_WIDTH); // channel 1, 50 Hz, 16-bit width
   ledcAttachPin(14, 1);   // GPIO 22 assigned to channel 1
   pinMode(LED_SET, OUTPUT);
}

void loop() {
   for (int i=COUNT_LOW ; i < COUNT_HIGH; i=i+100)
   {
      ledcWrite(1, i);       // sweep servo 1
      delay(50);
      Serial.println("servo: "+i);
   }
   digitalWrite(LED_SET, HIGH); 
  // turn the LED on (HIGH is the voltage level)
  delay(500);  
  digitalWrite(LED_SET, LOW);
  Serial.println("End");
  delay(3);  
}
