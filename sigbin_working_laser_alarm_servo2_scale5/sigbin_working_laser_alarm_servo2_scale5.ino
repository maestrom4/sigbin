
#define COUNT_LOW 0//servo
#define COUNT_HIGH 8888//servo
#define TIMER_WIDTH 16//servo
#include "esp32-hal-ledc.h" //servo
#include "HX711.h" //scale
float user_trash_weight;

//start of scale declaration
int scl1 = 26;
int scl2 = 27;
HX711 scale(scl1, scl2);
//end of scale declaration
/** speaker alarm and tripwire*/

int LED = 5;
int LaserSensor = 34;
int SensorReading = HIGH;  // HIGH MEANS NO OBSTACLE
int Laser = 2;
//int alarmSpeaker = 7;
//start of speaker
int freq = 2000;
int channel = 0;
int resolution = 8;
//end of speaker alarm and tripwire
// start of scale
int counter_weight = 20;
float calibration_factor = 119; // this calibration factor is adjusted according to my load cell
float units;
float units2; //temporary
float ounces;
//end of scale
void setup() {
  /*Scale*/
  Serial.begin(115200);
  Serial.println("HX711 calibration sketch");
  Serial.println("Remove all weight from scale");
  Serial.println("After readings begin, place known weight on scale");
  Serial.println("Press + or a to increase calibration factor");
  Serial.println("Press - or z to decrease calibration factor");
  scale.set_scale();
  scale.tare();  //Reset the scale to 0
  long zero_factor = scale.read_average(); //Get a baseline reading
  Serial.print("Zero factor: "); //This can be used to remove the need to tare the scale. Useful in permanent scale projects.
  Serial.println(zero_factor);

  /*Laser LED*/
  pinMode(LED, OUTPUT);
  pinMode(Laser, OUTPUT);
  pinMode(LaserSensor, INPUT);
  /*Speaker*/
  ledcSetup(channel, freq, resolution);
  ledcAttachPin(12, channel);

  /*SERVO*/
  ledcSetup(1, 50, TIMER_WIDTH); // channel 1, 50 Hz, 16-bit width
  ledcAttachPin(14, 1);   // GPIO 14 assigned to channel 1
}
void alarmFreq(){
  for (int freq = 0; freq < 4; freq = freq + 1){
   Serial.println(freq);
   ledcWriteTone(channel, 510);
   delay(500);
   ledcWriteTone(channel, 610);
   delay(500);
  }

}
void alarm() {
//  delay(3000); //Time before alarm starts
  alarmFreq();
  alarmFreq();
}
int laser_run(){
  int temp;
  digitalWrite(Laser, HIGH);
  delay(500);
  ledcWriteTone(channel, 0);
  temp = digitalRead(LaserSensor);
  return temp;

}

/*Starts check if value is between range of 9g and 60g*/
void run_scale_check(){
  boolean recheck=false;
  do{
  scale.set_scale(calibration_factor); //Adjust to this calibration factor
  Serial.println("Reading: ");
  units = scale.get_units(), 10;
  delay(500);
  if (units < 0)
  {
    units = 0.00;
  }
  Serial.print(units);
  Serial.print(" grams");
  Serial.print(" calibration_factor: ");
  Serial.println(calibration_factor);
  Serial.println("Searching Plastic bottle!");
//  delay(300);
  }while(!(units>9 && units<60)); //counts value until it reaches 20 increments
  Serial.println("Plastic bottle found");
}
/*Starts check if value is between range of 9g and 60g*/
/*Starts average unit */
float run_scale_ave(){
  float sum = 0;
  float tempcnt = 0;
  float tempave = 0;
  do{
    scale.set_scale(calibration_factor); //Adjust to this calibration factor
    Serial.println("Reading: ");
    units = scale.get_units(), 10;
    if (units < 0)
    {
      units = 0.00;
    }
    Serial.print(units);
    Serial.print(" grams");
    Serial.print(" calibration_factor: ");
    Serial.print(calibration_factor);
//    Serial.print(" ");
//    Serial.print(tempcnt);

    Serial.println();
    Serial.println("Searching Plastic bottle!");
    sum+=units;
    Serial.println("sum: "+String(sum));
    tempcnt++;
    delay(500);
  }while(tempcnt!=10);
  tempave=float(sum)/float(10.0);
  return tempave;
}
void run_servo(){
  for (int i=COUNT_LOW ; i < COUNT_HIGH ; i=i+100)
  {
    Serial.println("servo: "+i);
    ledcWrite(1, i);       // sweep servo 1
    delay(60);
  }
  
}
void loop() {
  digitalWrite(Laser, HIGH);
  run_scale_check();
  Serial.print("starts averages 20 units");
 
  user_trash_weight = run_scale_ave();
  Serial.println("trash ave unit: ");
  Serial.print(String(user_trash_weight));
  delay(5000);
  
  do{
    SensorReading = laser_run();
    Serial.println("Reading: "+SensorReading);
    if (SensorReading == LOW)
    {
      digitalWrite(LED, HIGH);
      Serial.println("alarm now!");
      alarm();
      SensorReading = HIGH;
    }
  
    else
    {
      //if no alarm move the servo if plastic cya
      digitalWrite(LED, LOW);
      Serial.println("No alarm move the servo");
      delay(1500);
      run_servo();
    }
  }while(SensorReading == HIGH);
  Serial.println("END");
  delay(5000);
  ESP.restart();
//  SensorReading = laser_run();
//  Serial.println("Reading: "+SensorReading);
//  if (SensorReading == LOW)
//  {
//    digitalWrite(LED, HIGH);
//    Serial.println("alarm now!");
//    alarm();
//    SensorReading = HIGH;
//  }
//
//  else
//  {
//    //if no alarm move the servo if plastic cya
//    digitalWrite(LED, LOW);
//    Serial.println("No alarm move the servo");
//    delay(1500);
//    run_servo();
//  }
}

