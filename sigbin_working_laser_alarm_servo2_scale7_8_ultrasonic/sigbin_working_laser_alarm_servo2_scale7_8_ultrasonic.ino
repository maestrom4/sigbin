
#define COUNT_LOW 0//servo
#define COUNT_HIGH 8888//servo
#define TIMER_WIDTH 16//servo
#include "esp32-hal-ledc.h" //servo
#include "HX711.h" //scale
int user_trash_weight;

//start of scale declaration
int scl1 = 33;
int scl2 = 32;
HX711 scale(scl1, scl2);
//end of scale declaration
/** speaker alarm and tripwire*/
  int LEDyellow = 16;
  int LED = 17;
  //int LED2 = 5;
  int LaserSensor = 12;
 // HIGH MEANS NO OBSTACLE
  int Laser = 13;
//int alarmSpeaker = 7;
//start of speaker
  int freq = 2000;
  int channel = 0;
  int resolution = 8;
  int speakerpin = 15;
  int servopin = 14;
  int servochannel = 7;
//end of speaker alarm and tripwire
// start of scale
  int counter_weight = 20;
  float calibration_factor = 232; // this calibration factor is adjusted according to my load cell
  float none_scale = 1.5; //weight of none scale part or the actual plaform
  float units;
  float units2; //temporary
  float ounces;
  //end of scale
/*Elapsed time*/
  //unsigned long startTime; //pra elapse time
  int interval=15000;
  // Tracks the time since last event fired
  unsigned long previousMillis=0;
/*Ultrasonic*/
  // defines pins numbers
  const int trigPin = 27;
  const int echoPin = 26;
  // defines variables
  long duration;
  int distance;
  long inches;
void setup() {
  /*Scale*/
 
  Serial.begin(115200);
  scale.set_scale();
  scale.tare();  //Reset the scale to 0
  long zero_factor = scale.read_average(); //Get a baseline reading
  Serial.print("Zero factor: "); //This can be used to remove the need to tare the scale. Useful in permanent scale projects.
  Serial.println(zero_factor);
  /*Adjust to this calibration factor*/
  scale.set_scale(calibration_factor);
  /*Laser LED*/
  
  pinMode(LEDyellow, OUTPUT);
  pinMode(LED, OUTPUT);
  pinMode(Laser, OUTPUT);
  pinMode(LaserSensor, INPUT);
  /*Speaker*/
  ledcSetup(channel, freq, resolution);
  ledcAttachPin(speakerpin, channel);

  /*SERVO*/
  ledcSetup(servochannel, 50, TIMER_WIDTH); // channel 1, 50 Hz, 16-bit width
  ledcAttachPin(servopin, servochannel);   // GPIO 14 assigned to channel 1
  /*Ultrasonic setup*/
  pinMode(trigPin, OUTPUT); // Sets the trigPin as an Output
  pinMode(echoPin, INPUT); // Sets the echoPin as an Input
  /*Calibration warning*/
  alarmCalibration();
}
void alarmFreq(){
  for (int freq = 0; freq < 4; freq = freq + 1){
   Serial.println(freq);
   ledcWriteTone(channel, 510);
   delay(500);
   ledcWriteTone(channel, 610);
   delay(500);
  }
  delay(2000);
  ledcWriteTone(channel, 0);
}
void alarmCalibration(){
  for (int freq = 0; freq < 4; freq = freq + 1){
   Serial.println(freq);
   ledcWriteTone(channel, 910);
   digitalWrite(LEDyellow, HIGH);
   delay(100);
  }
  delay(2000);
  ledcWriteTone(channel, 0);
  digitalWrite(LEDyellow, LOW);
}
boolean laser_run(){
  boolean temp;
  digitalWrite(Laser, HIGH);
  delay(500);
  ledcWriteTone(channel, 0);
  temp = digitalRead(LaserSensor);
  return temp;

}

/*Starts check if value is between range of 9g and 60g*/
void run_scale_check(){
  boolean recheck=false;
  do{

    Serial.println("Reading: ");
    units = scale.get_units(), 1;
    delay(1000);
    if (units < 0)
    {
      units = 0.00;
    }
    else 
    {
      units = units-none_scale;
    }
    Serial.print(units);
    Serial.println(" grams");
    Serial.println("Searching Plastic bottle!");
  }while(!(units>9 && units<60)); //counts value until it reaches 20 increments
  
}
/*Starts check if value is between range of 9g and 60g*/
/*Starts average unit */
int run_scale_ave(){
  float sum = 0;
  float tempcnt = 0;
  float tempave = 0;
  do{
    Serial.println("Reading: ");
    units = scale.get_units(), 1;
    if (units < 0)
    {
      units = 0.00;
    }
    else 
    {
      units = units-none_scale;
    }
    Serial.print(units);
    Serial.print(" grams");
    Serial.println("Searching Plastic bottle!");
    sum+=units;
    tempcnt++;
  }while(tempcnt!=10);
  tempave=float(sum)/float(10.0);
  return round(tempave);
}
void run_servo(){
  for (int i=COUNT_LOW ; i < COUNT_HIGH ; i=i+100)
  {
    Serial.println("servo: "+i);
    ledcWrite(servochannel, i);       // sweep servo 1
    delay(50);
  }
}
long microsecondsToInches(long microseconds)
{
  // According to Parallax's datasheet for the PING))), there are
  // 73.746 microseconds per inch (i.e. sound travels at 1130 feet per
  // second).  This gives the distance travelled by the ping, outbound
  // and return, so we divide by 2 to get the distance of the obstacle.
  // See: http://www.parallax.com/dl/docs/prod/acc/28015-PING-v1.3.pdf
  return microseconds / 74 / 2;
}
void loop() {
//  digitalWrite(Laser, HIGH);
  run_scale_check();
  delay(2000);
  int user_trash_weight = run_scale_ave();
  Serial.println("trash ave unit: ");
  Serial.println(String(user_trash_weight));
  delay(1000);
  
  /*Start of laser*/
  digitalWrite(Laser, HIGH);
  delay(500);
//  Serial.println("LaserSensor: "+String(digitalRead(LaserSensor)));
  if (digitalRead(LaserSensor) == LOW)
  { 
    digitalWrite(LED, HIGH);
      alarmFreq();
      alarmFreq();
  }
  else
  {
    
    run_servo();
  }
  digitalWrite(LED, LOW);
  digitalWrite(Laser, LOW);
  Serial.println("END");
  unsigned long currentMillis = millis();
  /*Ultrasonic initialize*/
  // Clears the trigPin
  digitalWrite(trigPin, LOW);
  delayMicroseconds(2);

   // How much time has passed, accounting for rollover with subtraction!
   if ((unsigned long)(currentMillis - previousMillis) >= interval) {
      // It's time to do something!
      /*Ultrasonic starts*/
      // Sets the trigPin on HIGH state for 10 micro seconds
      digitalWrite(trigPin, HIGH);
      delayMicroseconds(10);
      digitalWrite(trigPin, LOW);
      // Reads the echoPin, returns the sound wave travel time in microseconds
      duration = pulseIn(echoPin, HIGH);
      // Calculating the distance
      distance= duration*0.034/2;
      // Prints the distance on the Serial Monitor
      Serial.print("Distance in inches: ");
      inches = microsecondsToInches(duration);
      Serial.println(inches);
      
      Serial.println("15 seconds passed!");
      Serial.print("seconds passed:");
      Serial.println(millis());
      // Use the snapshot to set track time until next event
      previousMillis = currentMillis;
      delay(2000);
   }
  delay(2000);
  
  
}

