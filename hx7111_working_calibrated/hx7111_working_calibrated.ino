/*
 Setup your scale and start the sketch WITHOUT a weight on the scale
 Once readings are displayed place the weight on the scale
 Press +/- or a/z to adjust the calibration_factor until the output readings match the known weight
 Arduino pin 6 -> HX711 CLK
 Arduino pin 5 -> HX711 DOUT
 Arduino pin 5V -> HX711 VCC
 Arduino pin GND -> HX711 GND 
*/

#include "HX711.h"
#include "soc/rtc.h"
HX711 scale(33, 32);

//2230
int counter_weight = 20;
float calibration_factor = 200; // this calibration factor is adjusted according to my load cell
float units;
float ounces;

void setup() {
  rtc_clk_cpu_freq_set(RTC_CPU_FREQ_80M);
  Serial.begin(115200);
  Serial.println("HX711 calibration sketch");
  Serial.println("Remove all weight from scale");
  Serial.println("After readings begin, place known weight on scale");
  Serial.println("Press + or a to increase calibration factor");
  Serial.println("Press - or z to decrease calibration factor");

  scale.set_scale();
  scale.tare();  //Reset the scale to 0

  long zero_factor = scale.read_average(); //Get a baseline reading
  Serial.print("Zero factor: "); //This can be used to remove the need to tare the scale. Useful in permanent scale projects.
  Serial.println(zero_factor);
//  scale.set_scale(calibration_factor);
}

void loop() {

  //Adjust to this calibration factor
  scale.set_scale(calibration_factor); 
 
  Serial.print("Reading: ");
  units = scale.get_units(), 1;
//  if (units < 0)
//  {
//    units = 0.00;
//  }
//  ounces = units * 0.035274;
  /*If ang plastic is greater than 60g and less than 8g dle cya plastic*/
//  Serial.println(units>9);   
//  if(units>9 && units<60){
//     
//    Serial.println("plastic bottle: ");   
//    Serial.print(units); 
//    
//  }

  
  Serial.print(units);
  Serial.print(" grams"); 
  Serial.print(" calibration_factor: ");
  Serial.print(calibration_factor);
  Serial.println();
  delay(300);
//
//  if(Serial.available())
//  {
//    char temp = Serial.read();
//    if(temp == '+' || temp == 'a')
//      calibration_factor += 1;
//    else if(temp == '-' || temp == 'z')
//      calibration_factor -= 1;
//  }

}
