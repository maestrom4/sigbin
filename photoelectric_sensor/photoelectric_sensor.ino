/*
  IR Proximity Sensor interface code
  Turns on an LED on when obstacle is detected, else off.
  blog.circuits4you.com 2016
 */


const int ProxSensor=39;
const int LED = 5;
void setup() {                
  // initialize the digital pin as an output.
  // Pin 13 has an LED connected on most Arduino boards:
  pinMode(LED, OUTPUT);     
  //Pin 2 is connected to the output of proximity sensor
  pinMode(ProxSensor,INPUT);
  Serial.begin(115200);
}

void loop() {
  int x = digitalRead(ProxSensor);
  if(x==HIGH)      //Check the sensor output
  {
    digitalWrite(LED, HIGH);   // set the LED on
    Serial.print("Value: ");
    Serial.println(x);
  }
  else
  {
    digitalWrite(LED, LOW);    // set the LED off
    Serial.print("Value: ");
    Serial.println(x);
  }
  delay(500);              // wait for a second
}
