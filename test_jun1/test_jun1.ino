// Select your modem:
//#define TINY_GSM_MODEM_SIM800
//#define TINY_GSM_MODEM_SIM900
#define TINY_GSM_MODEM_A6
//#define TINY_GSM_MODEM_M590

#define _DEBUG_

#include <TinyGsmClient.h>
#include <SoftwareSerial.h>
#include <ThingerGSM.h>

#define USERNAME ""
#define DEVICE_ID ""
#define DEVICE_CREDENTIAL ""

SoftwareSerial SerialAT(3, 1); // RX, TX

ThingerGPRS thing(USERNAME, DEVICE_ID, DEVICE_CREDENTIAL, SerialAT);

void setup() {
  // for logging
  Serial.begin(115200); 

   // SerialAT
  SerialAT.begin(38400);

  // set APN
  thing.setAPN("orangeworld", "", "");

  // set PIN (optional)
  // thing.setPin("1234");
  
  pinMode(13, OUTPUT);

}

void loop() {
  thing.handle();
}
